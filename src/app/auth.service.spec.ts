import { TestBed, inject } from '@angular/core/testing';
import { HttpModule, ResponseOptions, Response, XHRBackend } from '@angular/http';
import { MockBackend, MockConnection } from '@angular/http/testing';

import { AuthService } from './auth.service';

describe('AuthService', () => {
  let service: AuthService;
  let backend: MockBackend;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpModule
      ],
      providers: [
        AuthService,
        { provide: XHRBackend, useClass: MockBackend }
      ]
    });
  });

  beforeEach(inject([AuthService, XHRBackend], (newService: AuthService, mockBackend: MockBackend) => {
   service = newService;
   backend = mockBackend;
  }));

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('login', () => {
    it('should send a request', () => {
      backend.connections.subscribe((connection: MockConnection) => {
        let options = new ResponseOptions({
          body: JSON.stringify(true)
        });
        connection.mockRespond(new Response(options));
      });

      service.login('user', 'pass').subscribe((response) => {
        expect(response.json()).toEqual(true);
      });
    });
  });

  describe('getToken', () => {
    it('should return previously set token', () => {
      const token = 'token';

      service.setToken(token);
      
      expect(service.getToken()).toEqual(token);
    });
  });
});
