import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class AuthService {
  private loginUrl = 'api/users/login';
  private token;

  constructor(private http: Http) { }

  login(userName: String, password: String) {
    return this.http.post(
      this.loginUrl,
      {
        login: userName,
        password: password
      }
    );
  }

  getToken() {
    return this.token;
  }

  setToken(token) {
    this.token = token;
  }

}
