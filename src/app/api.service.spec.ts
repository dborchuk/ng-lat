import { TestBed, inject } from '@angular/core/testing';
import { HttpModule, ResponseOptions, Response, XHRBackend } from '@angular/http';
import { MockBackend, MockConnection } from '@angular/http/testing';

import { ApiService } from './api.service';
import { AuthService } from './auth.service';

class AuthServiceMock {
  getToken() {}
}

describe('ApiService', () => {
  let service: ApiService;
  let backend: MockBackend;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpModule],
      providers: [
        ApiService,
        { provide: XHRBackend, useClass: MockBackend },
        { provide: AuthService, useClass: AuthServiceMock }
      ]
    });
  });

  beforeEach(inject([ApiService, XHRBackend], (newService: ApiService, mockBackend: MockBackend) => {
   service = newService;
   backend = mockBackend;
  }));

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('checkToken', () => {
    it('should send a request', () => {
      backend.connections.subscribe((connection: MockConnection) => {
        let options = new ResponseOptions({
          body: JSON.stringify({ success: true })
        });
        connection.mockRespond(new Response(options));
      });

      service.checkToken().subscribe((response) => {
        expect(response.json()).toEqual({ success: true });
      });
    });
  });

  describe('rand', () => {
    it('should send a request', () => {
      backend.connections.subscribe((connection: MockConnection) => {
        let options = new ResponseOptions({
          body: JSON.stringify(42)
        });
        connection.mockRespond(new Response(options));
      });

      service.rand().subscribe((response) => {
        expect(response.json()).toEqual(42);
      });
    });
  });
});
