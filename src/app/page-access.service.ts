import { Injectable } from '@angular/core';

@Injectable()
export class PageAccessService {
  accessibleForAll = [
    "/login"
  ];

  constructor() { }

  hasAccess(url: string, token: String): boolean {
    return !!token || this.accessibleForAll.indexOf(url) >= 0;
  }

}
