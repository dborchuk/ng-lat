import { TestBed, inject } from '@angular/core/testing';

import { PageAccessService } from './page-access.service';

describe('PageAccessService', () => {
  let service: PageAccessService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PageAccessService]
    });
  });

  beforeEach(inject([PageAccessService], (newService: PageAccessService) => {
   service = newService;
  }));

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('hasAccess', () => {
    it('should return true', () => {
      const token = 'token';
      const url = '/login';

      expect(service.hasAccess(url, token)).toEqual(true);
      expect(service.hasAccess(url, null)).toEqual(true);
    });
  });

  describe('hasAccess', () => {
    it('should return false', () => {
      const url = '/dashboard';

      expect(service.hasAccess(url, null)).toEqual(false);
    });
  });
});
