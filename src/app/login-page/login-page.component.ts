import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {
  username: String;
  password: String;
  errorMessage: String;
  isFetching: Boolean;
  
  constructor(
    private authService: AuthService,
    private router: Router
  ) {
    this.authService.setToken(null);
  }

  login(userName: String, password: String) {
    this.isFetching = true;
    this.authService.login(userName, password).subscribe(response => {
      let data = response.json();
      this.isFetching = false;
      this.authService.setToken(data.token);
      this.router.navigateByUrl('/');
    }, (err) => {
      this.isFetching = false;
      this.errorMessage = err.json().error;
    });
  }

  ngOnInit() {
  }

}
