import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';

import { LoginPageComponent } from './login-page.component';
import { MockComponent } from '../helpers/mock-component';
import { AuthService } from '../auth.service';

class AuthServiceMock {
  setToken() {}
  login() {}
}

describe('LoginPageComponent', () => {
  let component: LoginPageComponent;
  let fixture: ComponentFixture<LoginPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        RouterTestingModule
      ],
      declarations: [
        LoginPageComponent,
        MockComponent({ selector: 'md-spinner' }),
        MockComponent({ selector: 'md-card-title' }),
        MockComponent({ selector: 'md-input-container' }),
        MockComponent({ selector: 'md-card-content' }),
        MockComponent({ selector: 'md-card-actions' }),
        MockComponent({ selector: 'md-card' })
      ],
      providers: [
        { provide: AuthService, useClass: AuthServiceMock }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  describe('login', () => {
    it('should redirect to "/"', () => {
      let router = fixture.debugElement.injector.get(Router);
      let authService = fixture.debugElement.injector.get(AuthService);
      let data = {
        token: 'token'
      };
      spyOn(authService, 'login').and.callFake(() => {
        return Observable.from([{
          json: () => {
            return data;
          }
        }]);
      });
      spyOn(router, 'navigateByUrl');

      component.login('user', 'pass');

      expect(router.navigateByUrl).toHaveBeenCalledWith('/');
    });

    it('should show error message', () => {
      let authService = fixture.debugElement.injector.get(AuthService);
      let errorMessage = 'error';
      let error = {
          json: () => {
            return {error: errorMessage};
          }
        };
      spyOn(authService, 'login').and.callFake(() => {
        return Observable.throw(error);
      });

      component.login('user', 'pass');

      expect(component.errorMessage).toBe(errorMessage);
    });
  });
});
