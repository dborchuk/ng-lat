import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule }   from '@angular/router';
import { 
  MdInputModule,
  MdCardModule,
  MdButtonModule,
  MdProgressSpinnerModule,
  MdDialogModule
} from '@angular/material';

import { AppComponent } from './app.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { DashboardPageComponent } from './dashboard-page/dashboard-page.component';
import { AlertDialogComponent } from './alert-dialog/alert-dialog.component';
import { AuthService } from './auth.service';
import { AuthGuard } from './auth-guard.service';
import { PageAccessService } from './page-access.service';
import { ApiService } from './api.service';

@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    DashboardPageComponent,
    AlertDialogComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    BrowserAnimationsModule,
    MdInputModule,
    MdCardModule,
    MdButtonModule,
    MdProgressSpinnerModule,
    MdDialogModule,
    RouterModule.forRoot([
      {
        path: 'login',
        component: LoginPageComponent
      },
      {
        path: 'dashboard',
        component: DashboardPageComponent,
        canActivate: [AuthGuard]
      },
      { path: '**', redirectTo: '/dashboard' }
    ])
  ],
  providers: [
    AuthService,
    AuthGuard,
    PageAccessService,
    ApiService
  ],
  bootstrap: [AppComponent],
  entryComponents: [AlertDialogComponent]
})
export class AppModule { }
