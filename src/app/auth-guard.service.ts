import { Injectable } from '@angular/core';
import { 
  RouterStateSnapshot,
  ActivatedRouteSnapshot,
  CanActivate,
  Router
} from '@angular/router';

import { AuthService } from './auth.service';
import { PageAccessService } from './page-access.service';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(
    private authService: AuthService,
    private pageAccessService: PageAccessService,
    private router: Router
  ) { }

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ) {
    const hasAccess = this.pageAccessService.hasAccess(state.url, this.authService.getToken());

    if (!hasAccess) {
      this.router.navigateByUrl('/login');
    }

    return hasAccess;
  }
}
