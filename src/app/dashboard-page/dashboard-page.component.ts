import { Component, OnInit } from '@angular/core';
import { MdDialog } from '@angular/material';
import { Router } from '@angular/router';

import { ApiService } from '../api.service';
import { AlertDialogComponent } from '../alert-dialog/alert-dialog.component';

@Component({
  selector: 'app-dashboard-page',
  templateUrl: './dashboard-page.component.html',
  styleUrls: ['./dashboard-page.component.css']
})
export class DashboardPageComponent implements OnInit {
  isFetching = false;

  constructor(
    private apiService: ApiService,
    private dialog: MdDialog,
    private router: Router
  ) { }

  checkToken() {
    this.isFetching = true;
    this.apiService.checkToken().subscribe(() => {
      this.isFetching = false;
      let dialog = this.dialog.open(AlertDialogComponent);
      dialog.componentInstance.text = 'Token is valid.';
    }, () => {
      this.isFetching = false;
      this.router.navigateByUrl('/login');
    });
  }

  showRandom() {
    this.isFetching = true;
    this.apiService.rand().subscribe((response) => {
      this.isFetching = false;
      const data = response.json();
      const dialog = this.dialog.open(AlertDialogComponent);
      dialog.componentInstance.text = 'Rand is ' + data.res;
    }, () => {
      this.isFetching = false;
      this.router.navigateByUrl('/login');
    });
  }

  ngOnInit() {
  }

}
