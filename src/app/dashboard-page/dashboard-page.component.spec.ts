import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpModule } from '@angular/http';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';

import { MdDialog } from '@angular/material';
import { RouterTestingModule } from '@angular/router/testing';

import { MockComponent } from '../helpers/mock-component';
import { DashboardPageComponent } from './dashboard-page.component';
import { ApiService } from '../api.service';
import { AuthService } from '../auth.service';

class ApiServiceMock {
  checkToken() {
    return Observable.from([{}]);
  }
  rand() {}
}
class AuthServiceMock {}
class MdDialogMock {
  open(obj) {
    return {
      componentInstance: new obj()
    }
  }
}

describe('DashboardPageComponent', () => {
  let component: DashboardPageComponent;
  let fixture: ComponentFixture<DashboardPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpModule,
        RouterTestingModule
      ],
      declarations: [
        DashboardPageComponent,
        MockComponent({ selector: 'md-spinner' })
      ],
      providers: [
        { provide: ApiService, useClass: ApiServiceMock },
        { provide: AuthService, useClass: AuthServiceMock },
        { provide: MdDialog, useClass: MdDialogMock }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DashboardPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });

  describe('checkToken', () => {
    it('should show the dialog', () => {
      let dialogInstance = {
        componentInstance: {
          text: ''
        }
      };
      let dialog = fixture.debugElement.injector.get(MdDialog);
      spyOn(dialog, 'open').and.callFake(() => {
        return dialogInstance;
      });
      component.checkToken();

      expect(dialog.open).toHaveBeenCalled();
      expect(dialogInstance.componentInstance.text).toBe('Token is valid.');
    });

    it('should lead to login page', () => {
      let dialog = fixture.debugElement.injector.get(MdDialog);
      let router = fixture.debugElement.injector.get(Router);
      let apiService = fixture.debugElement.injector.get(ApiService);
      spyOn(apiService, 'checkToken').and.callFake(() => {
        return Observable.throw(new Error());
      });
      spyOn(dialog, 'open');
      spyOn(router, 'navigateByUrl');

      component.checkToken();

      expect(dialog.open).not.toHaveBeenCalled();
      expect(router.navigateByUrl).toHaveBeenCalledWith('/login');
    });
  });

  describe('showRandom', () => {
    it('should show the dialog', () => {
      let dialogInstance = {
        componentInstance: {
          text: ''
        }
      };
      let dialog = fixture.debugElement.injector.get(MdDialog);
      let apiService = fixture.debugElement.injector.get(ApiService);
      let data = {
        res: 42
      };
      spyOn(dialog, 'open').and.callFake(() => {
        return dialogInstance;
      });
      spyOn(apiService, 'rand').and.callFake(() => {
        return Observable.from([{
          json: () => {
            return data;
          }
        }]);
      });
      component.showRandom();

      expect(dialog.open).toHaveBeenCalled();
      expect(dialogInstance.componentInstance.text).toBe('Rand is ' + data.res);
    });

    it('should lead to login page', () => {
      let dialog = fixture.debugElement.injector.get(MdDialog);
      let router = fixture.debugElement.injector.get(Router);
      let apiService = fixture.debugElement.injector.get(ApiService);
      spyOn(apiService, 'rand').and.callFake(() => {
        return Observable.throw(new Error());
      });
      spyOn(dialog, 'open');
      spyOn(router, 'navigateByUrl');

      component.showRandom();

      expect(dialog.open).not.toHaveBeenCalled();
      expect(router.navigateByUrl).toHaveBeenCalledWith('/login');
    });
  });
});
