import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

import { AuthService } from './auth.service';

@Injectable()
export class ApiService {
  private checkTokenUrl = 'api/fake/check';
  private randUrl = 'api/fake/rand';

  constructor(private http: Http, private authService: AuthService) { }

  private getHeaders() {
    let headers = new Headers();
    headers.append('Authorization', this.authService.getToken());

    return headers;
  }

  checkToken() {
    return this.http.post(
      this.checkTokenUrl,
      null,
      { headers: this.getHeaders() }
    );
  }

  rand() {
    return this.http.post(
      this.randUrl,
      null,
      { headers: this.getHeaders() }
    );
  }

}
