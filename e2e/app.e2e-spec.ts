import { NgLatPage } from './app.po';

describe('ng-lat App', () => {
  let page: NgLatPage;

  beforeEach(() => {
    page = new NgLatPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
